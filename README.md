# N8N Local Development

This is the n8n local development environment which allows to easily develop the N8N Nodes which are designed as npm packages. 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Installing

A step by step series of examples that tell you how to get a development env running

To get started just run npm install

```
npm install
```

To connect the N8N Nodes package run

```
npm link YOUR_N8N_NODES_PACKAGE_NAME
```

Setup the PostgreSQL Database:

Go to [https://www.postgresql.org/download/](https://www.postgresql.org/download/) or [https://postgresapp.com/](https://postgresapp.com/). Additionally you may install the app to manage the Database. Besides that, you need to follow instructions there and save the database access credentials. 


Copy the env.sample to .env and set the connection details to your PostgreSQL Database:
```
cp env.sample .env
```

Set the PostgreSQL Database credentials into the .env file:
```
nano .env
```

To start working run
```
npm start
```

## Available Scripts

In the project directory, you can run:
### `npm start`

Runs the app in the development mode, which tracks the changes.
Open [http://localhost:5678](http://localhost:5678) to view it in the browser.

The page won't reload if you make edits and will lose the connection with n8n until build is done, so you may need to wait and reload it manually.
You will also see any lint errors in the console.

## Learn more

* [N8N](https://docs.n8n.io/) - This is n8n documentation. 
* [N8N Development](https://docs.n8n.io/#/create-node) - This is n8n Node development documentation, which may be useful when working on this project. 
* [PostgreSQL](https://www.postgresql.org/) - The PostgreSQL documentation. 
